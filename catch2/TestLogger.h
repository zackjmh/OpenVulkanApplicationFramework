#pragma once
#ifndef OVGF_LOGGER_H
#define OVGF_LOGGER_H
#include <Logger.h>
#include <string>

namespace IH_Utils {
	namespace Test {
		using std::string;

		inline void Test_Logger(string message,
			LogLevel level = LogLevel::DETAIL) {
#ifndef NLOG
			(Logger::get_logger("TestLog.txt"))(move(message), level);
#endif
		}
	}
}
#endif // !OVGF_LOGGER
