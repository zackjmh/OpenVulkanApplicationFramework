#include "Worker.h"

namespace ThreadCore {

	size_t Worker::worker_count() {
		static size_t worker_counter = 0;
		return worker_counter++;

	}

	Worker::Worker() : worker_id(worker_count()),
		alive_(true),
		j_count(0),
		r_count(0){
	};

	Worker::~Worker() {
	};

	double_t Worker::get_transform() const {
		return transform;
	};

	void Worker::set_transform(double_t ntransform) noexcept {
		transform = ntransform;
	}; 


	uint64_t Worker::run_count() const {
		return r_count;
	};

	void Worker::to_run(uint64_t arg) {
		r_count += arg;
	}

	
	void Worker::kill() {
		alive_ = false;
	}

	bool Worker::alive() const {
		return alive_;
	}


	uint64_t Worker::job_count(bool report /*default false*/) const {
#ifndef NDEBUG
		if (report) printf("Worker %llu ran %llu times.\n", worker_id, j_count);
#endif
		return j_count;
	}

}