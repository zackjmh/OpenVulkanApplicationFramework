#pragma once
#ifndef JOB_TEST_H
#define JOB_TEST_H

#include <math.h>
#include "Worker.h"

class Next {
public:
	Next() {};

	Next(ThreadCore::Worker_t worker) {
		exec(std::forward<ThreadCore::Worker_t>(worker));
	};
	~Next() {};
	inline void exec(ThreadCore::Worker_t worker) {
		worker.set_transform(std::pow(std::pow(worker.get_transform(), 2), 2));
	}

};

class Test {
public:
	Test() {};
	~Test() {
	};

	inline void exec(ThreadCore::Worker_t worker) {
		worker.set_transform(std::pow(std::pow(worker.get_transform(), 2), 2));
		auto n = [](ThreadCore::Worker_t worker) {
			worker.set_transform(worker.get_transform() * 2);
		};
		n(std::forward<ThreadCore::Worker_t>(worker));
		Next nx(std::forward<ThreadCore::Worker_t>(worker));
	}

};



#endif
