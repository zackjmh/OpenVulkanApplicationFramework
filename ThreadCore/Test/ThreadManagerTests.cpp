#include "catch.hpp"
#include <TestLogger.h>
#include <CalcUtils.h>

#include <ThreadManager.h>


using IH_Utils::Test::Test_Logger;
using IH_Utils::Calc::get_ops;
using IH_Utils::Calc::get_time;

using std::to_string;

using std::chrono::high_resolution_clock;
using std::chrono::nanoseconds;
using std::chrono::duration_cast;

SCENARIO("A Thread Manager can process jobs with an operator()(worker_t) method", "[thread_manager][threadcore]") {
	GIVEN("A Thread Manager with no initial jobs ") {

		ThreadCore::ThreadManager tm{};
		const auto dummy = [](ThreadCore::Worker_t w) {};
		const auto plus_one = [](ThreadCore::Worker_t w) {w.set_transform(w.get_transform() + 1); };
		const auto sub_one = [](ThreadCore::Worker_t w) {w.set_transform(w.get_transform() - 1); };

		WHEN("No jobs are queued ") {
			THEN("The threadmanager is idle, sum is 0, and MAX_THREADS is equal to hardware_concurrency()") {
				REQUIRE(tm.is_idle());
				REQUIRE(tm.sum() == 0);
				REQUIRE(tm.MAX_THREADS == std::thread::hardware_concurrency());
			}
		}

		WHEN("A single job is queued and waited on ") {
			const size_t EXPECTED = 1;

			tm.queue_job(dummy, EXPECTED);
			
			while (!tm.is_idle()) {

			}
			
			THEN("Sum is equal to one ") {
				REQUIRE(tm.is_idle());
				REQUIRE(tm.sum() == EXPECTED);
			}
		}

		WHEN("Many of the same job are queued and waited on ") {
			const size_t EXPECTED = 100;

			tm.queue_job(dummy, EXPECTED);
			while (!tm.is_idle()) {

			}

			THEN("Sum is equal to the number of jobs queued ") {
				REQUIRE(tm.is_idle());
				REQUIRE(tm.sum() == EXPECTED);
			}
		}

		WHEN("Several different jobs are queued ") {
			const size_t EXPECTED = 1;

			tm.queue_job(dummy, EXPECTED);
			tm.queue_job(plus_one, EXPECTED);
			tm.queue_job(sub_one, EXPECTED);

			while (!tm.is_idle()) {

			}

			THEN("Sum is equal to the number of jobs queued ") {
				REQUIRE(tm.is_idle());
				REQUIRE(tm.sum() == EXPECTED * 3);
			}
		}

		WHEN("Many of several different jobs are queued ") {
			const size_t EXPECTED = 100;

			tm.queue_job(dummy, EXPECTED);
			tm.queue_job(plus_one, EXPECTED);
			tm.queue_job(sub_one, EXPECTED);

			while (!tm.is_idle()) {

			}

			THEN("Sum is equal to the number of jobs times the expected value ") {
				REQUIRE(tm.is_idle());
				REQUIRE(tm.sum() == EXPECTED * 3);
			}
		}

		WHEN("A large number of jobs are queued") {
			const size_t EXPECTED = std::tera::num;

			tm.queue_job(dummy, EXPECTED);

			while (!tm.is_idle()) {

			}

			THEN("Sum is equal to the number of jobs queued ") {
				REQUIRE(tm.is_idle());
				REQUIRE(tm.sum() == EXPECTED);
			}
		}

		WHEN("A large number of a variety of jobs are queued") {
			const size_t EXPECTED = std::tera::num;

			tm.queue_job(dummy, EXPECTED);
			tm.queue_job(sub_one, EXPECTED);
			tm.queue_job(plus_one, EXPECTED);

			while (!tm.is_idle()) {

			}

			THEN("Sum is equal to the number of jobs queued ") {
				REQUIRE(tm.is_idle());
				REQUIRE(tm.sum() == EXPECTED * 3);
			}
		}

	}
}
