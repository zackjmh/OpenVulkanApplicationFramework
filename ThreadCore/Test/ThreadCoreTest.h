#pragma once
#ifndef TC_BASE_TEST_H
#include <ThreadManager.h>
#include <TestLogger.h>

namespace ThreadCore {
	namespace Test {

		using IH_Utils::Test::Test_Logger;
		using IH_Utils::LogLevel;
		using std::to_string;

		class ThreadCoreTest {
		public:

			ThreadCoreTest() : tm{} {

			}

			bool setup_assert() noexcept {
				auto sum = tm.sum();
				
				if (sum) {
					Test_Logger("Err: ThreadManager setup failed! Expected sum of jobs to be 0, was " 
						+ to_string(sum), LogLevel::CRITICAL);
					return false;
				} else {
					Test_Logger("ThreadCore setup complete!");
					return true;
				}
			}

			virtual void test() noexcept = 0;

			virtual bool test_assert() noexcept = 0;

			virtual ~ThreadCoreTest() noexcept {

			}

		protected:
			typedef ThreadCoreTest super;
			ThreadCore::ThreadManager tm;
		};
	}
}
#endif // !TC_BASE_TEST_H
