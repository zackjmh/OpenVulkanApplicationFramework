#pragma once
#ifndef TC_FUTURES_TEST
#define TC_FUTURES_TEST
#include "ThreadCoreTest.h"

namespace ThreadCore {
	namespace Test {
		class ThreadcoreFuturesTest : public ThreadCoreTest {

			ThreadcoreFuturesTest(uint64_t) {};

			void test() {
				future = tm.queue_job(FutureJob{}, 1);

				while (!future) {}
			}

			bool test_assert() {
				return *future != nullptr;
			}

		protected:
			ThreadCore::future<int> future;
		};
	}
}

#endif