#pragma once
#ifndef JOB_ADD_WORK_H
#define JOB_ADD_WORK_H

#include <math.h>
#include <ratio>

#include "Job.h"
#include "ThreadManager.h"

namespace ThreadCore {
	template<typename JobAdd>
	class WorkJob {
		//Job producer job, mostly for testing
	public:


		WorkJob(ThreadCore::ThreadManager& tm_, uint64_t num_jobs) : tm(tm_), NUM_JOBS(num_jobs) {
		};

		~WorkJob() {
		};

		void exec() const {
			tm.queue_job(JobAdd(), NUM_JOBS);
		
		}

		void exec(Worker_t worker) const {
			tm.queue_job(JobAdd(), NUM_JOBS);
		}

	private:
		ThreadCore::ThreadManager&       tm;
		const uint64_t                   NUM_JOBS;

	};

}
#endif
