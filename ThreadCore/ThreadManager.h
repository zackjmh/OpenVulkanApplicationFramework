#pragma once
#ifndef IH_THREAD_MANG_H
#define IH_THREAD_MANG_H

#include <thread>
#include <vector>
#include <queue>
#include <unordered_map>
#include <iostream>
#include <math.h>
#include <typeindex>
#include "Job.h"


namespace ThreadCore {


	void calc_ops(uint64_t cur_jobs, uint64_t seen_jobs, std::chrono::nanoseconds duration);


	class ThreadManager {
		//Holds the thread_pool and distributes work nievly between the workers
		//TODO: Determine MAX_THREADS via dynamic benchmark
	public:
		ThreadManager();
		~ThreadManager();

		const size_t	MAX_THREADS;


		void queue_job(Job_t, uint64_t = 1); //offloads iteration responsibility to producer jobs

		void find_work(Worker_t); //looks through the job pool for work

		bool is_idle(); // currently pulling double duty as the OPS counter

		uint64_t sum(bool = false) const; //count executed jobs

	private:
		class Job_pool_d;
		std::vector<std::thread>                                   thread_pool;
		std::vector<Worker*>				                       worker_pool;
		std::unordered_map<std::type_index, Job_pool_d>            job_pool;
		std::mutex                                                 pool_create_lock;
		std::size_t                                                next_dispatch_;
		std::chrono::high_resolution_clock::time_point             start_time;
		bool running = true;
#ifndef NDEBUG
		std::uint64_t                                              job_count;
		inline void                                                frame_count(bool);
#endif
	};
}
#endif
