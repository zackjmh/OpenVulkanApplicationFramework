#pragma once
#ifndef IH_WORKER_H
#define IH_WORKER_H

#include <mutex>
#include <condition_variable>
#include <cassert>
#include <iostream>
#include <deque>

namespace ThreadCore {
	class Worker {
		//An object that can be shared between a thread and the job scheduler
		//Once it begins executing, it will attempt to empty it's own sub_buffer before looking for work in other sub-buffers
	public:
		using Shared_Worker_t = std::shared_ptr<Worker>; //Usual worker type
		using Shared_Const_Worker_t = std::shared_ptr<const Worker>; //Iterator type

		const size_t worker_id; //set at construction from worker_count


		Worker();

		~Worker();

		void kill(); //sets alive_ to false

		bool alive() const; //reports the state of alive_

		uint64_t job_count(bool = false) const; //reports j_count

		uint64_t run_count() const;

		inline void operator()() {
			++j_count;
			--r_count;
		}

		double_t get_transform() const;

		void set_transform(double_t) noexcept;

		void to_run(uint64_t); //this sucks, but when I re-wrote things to change the idiom here my CPU usage tanked

	private:
		static size_t worker_count(); //used to set worker id

		bool                             alive_;
		uint64_t                         j_count;
		uint64_t                         r_count;
		double_t                         transform;

	};

		using Const_Worker_t = const Worker&;
		using Worker_t = Worker&;
}
#endif
