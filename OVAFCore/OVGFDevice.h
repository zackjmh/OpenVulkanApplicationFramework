#pragma once
#ifndef OVGF_DEVICE_H
#define OVGF_DEVICE_H
#include <vulkan/vulkan.hpp>
#include "ovgf_ptr.h"

namespace OVGF {
	using std::string;
	using std::vector;

	class OVGFDevice {
	public:
		OVGFDevice() = delete;

		OVGFDevice(VkInstance&) noexcept;
		~OVGFDevice() noexcept {};


	private:
		VkPhysicalDevice p_device;
		VkDevice         l_device;
	};
}

#endif