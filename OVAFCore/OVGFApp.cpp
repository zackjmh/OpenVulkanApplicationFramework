#include "OVGFApp.h"
#include <fstream>

namespace OVGF {
	using std::ifstream;

	using std::runtime_error;

	OVGFApp::OVGFApp(string config_file) noexcept : 
							settings{ load_config(config_file) },
							window{ settings },
							renderer{ window, settings } 
	{
		OVGF_Logger("Starting app.");

	
	}

	OVGFSettings OVGFApp::load_config(string config_file, string delim) {

		ifstream config{ config_file };
		string tokens;
		SettingsMap out;

		while (getline(config, tokens)) {
			string setting = tokens.substr(0, tokens.find(delim));
			string value = tokens.substr(tokens.find(delim) + 1);

			if (out.count(setting)) {
				string err_str = "Found two entries for setting " + setting;
				assert(false && err_str.c_str());
			}

			out.emplace(setting, value);
		}



		return { move(out) };
	}

}