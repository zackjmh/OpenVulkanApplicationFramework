#include "OVGFVulkan.h"
#include "ovgf_ptr.h"
#include "OVGF_Logger.h"

namespace OVGF {
	using IH_Utils::LogLevel;

#ifndef NDEBUG
	const std::vector<const char*> validationLayers = {
		"VK_LAYER_LUNARG_standard_validation"
	};

	bool validation_support() {
		uint32_t layerCount;
		vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

		std::vector<VkLayerProperties> availableLayers(layerCount);
		vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

		for (const char* layerName : validationLayers) {
			bool layerFound = false;

			for (const auto& layerProperties : availableLayers) {
				if (strcmp(layerName, layerProperties.layerName) == 0) {
					layerFound = true;
					break;
				}
			}

			if (!layerFound) {
				return false;
			}
		}

		return true;
	}

	VkResult CreateDebugUtilsMessengerEXT(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pCallback) {
		auto func = (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");
		if (func != nullptr) {
			return func(instance, pCreateInfo, pAllocator, pCallback);
		} else {
			return VK_ERROR_EXTENSION_NOT_PRESENT;
		}
	}

	void DestroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT callback, const VkAllocationCallbacks* pAllocator) {
		auto func = (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");
		if (func != nullptr) {
			func(instance, callback, pAllocator);
		}
	}

	static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
		VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
		VkDebugUtilsMessageTypeFlagsEXT messageType,
		const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
		void* pUserData) {
		LogLevel level;

		if (messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT
			|| messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT) {
			level = LogLevel::DETAIL;
		} else if(messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT) {
			level = LogLevel::WARNING;
		} else if (messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT) {
			level = LogLevel::CRITICAL;
		}
		

		OVGF_Logger(string{ pCallbackData->pMessage }, level);

		return VK_FALSE;
	}

	void OVGFVulkan::setup_OVGF_debug_callback() noexcept {

#ifndef NDEBUG
		if (!validation_support()) 
			OVGF_Logger("Validation layers requested but not supported.", LogLevel::CRITICAL);
#endif // !NDEBUG


		VkDebugUtilsMessengerCreateInfoEXT  createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
		createInfo.flags = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT;
		createInfo.pfnUserCallback = debugCallback;

		VkResult res = CreateDebugUtilsMessengerEXT(instance, &createInfo, OVGF_ALLOCATOR, &callback);

#ifndef NDEBUG
		if (res != VK_SUCCESS)
			OVGF_Logger("Failed to set up debug callback.", LogLevel::CRITICAL);
#endif // !NDEBUG

	}
#endif // !NDEBUG


	OVGFVulkan::OVGFVulkan(OVGFWindow& w, OVGFSettings& settings) noexcept : device() {

		VkResult res = make_instance(w, settings.app_name, settings.version);

#ifndef NDEBUG


		if (res != VK_SUCCESS) {
			OVGF_Logger("Failed to set up instance.", LogLevel::FATAL);
			abort();
		}

		setup_OVGF_debug_callback();
#endif

		SDL_bool res2 = w.get_surface(instance, surface);

#ifndef NEDBUG
		if (!res2) {
			OVGF_Logger("Failed to get surface.", LogLevel::FATAL);
			abort();
		}
#endif // !NEDBUG

		


	}



	OVGFVulkan::~OVGFVulkan() noexcept {

#ifndef NDEBUG
		DestroyDebugUtilsMessengerEXT(instance, callback, OVGF_ALLOCATOR);
#endif // !NDEBUG

		vkDestroyInstance(instance, OVGF_ALLOCATOR);
	}

	vector<OVGFDevice> OVGFVulkan::get_devices(OVGFSettings&) noexcept {
		vkEnumeratePhysicalDevices(instance, &device_count, nullptr);

		if (!device_count) {
			OVGF_Logger("ERROR: No Vulkan supporting devices found.", LogLevel::FATAL);
			abort();
		}

		vector<VkPhysicalDevice> temp_devices{device_count};

		return vector<OVGFDevice>{};
	}

	VkResult OVGFVulkan::make_instance(OVGFWindow& w, string& app_name, OVGFSettings::Version& ver) noexcept {
		VkApplicationInfo appInfo = {};
		appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
		appInfo.pApplicationName = app_name.c_str();
		appInfo.applicationVersion = VK_MAKE_VERSION(ver.major, ver.minor, ver.rev);
		appInfo.pEngineName = "OVGF";
		appInfo.engineVersion = VK_MAKE_VERSION(ver.major, ver.minor, ver.rev);
		appInfo.apiVersion = VK_API_VERSION_1_1;

		VkInstanceCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		createInfo.pApplicationInfo = &appInfo;
#ifndef NDEBUG
		createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
		createInfo.ppEnabledLayerNames = validationLayers.data();
#endif // !NDEBUG

		

		std::vector<const char*> exts;

		uint32_t num_ext = 0;

		w.get_window_extentions(num_ext, exts);

#ifndef NDEBUG
		exts.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
		++num_ext;
#endif // !NDEBUG


		createInfo.enabledExtensionCount = num_ext;
		createInfo.ppEnabledExtensionNames = exts.data();

		return vkCreateInstance(&createInfo, OVGF_ALLOCATOR, &instance);
	}


}