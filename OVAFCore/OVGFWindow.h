#pragma once
#ifndef OVGF_SDL_WINDOW_H
#define OVGF_SDL_WINDOW_H
#include <string>
#include <vector>
#include <SDL.h>
#include <SDL_vulkan.h>
#include "ovgf_ptr.h"
#include "OVGFConfig.h"

namespace OVGF {
	using std::string;
	using std::vector;

	class OVGFWindow {
	public:
		using Window_t = std::unique_ptr<SDL_Window>;
		using Const_Window_t = std::unique_ptr<const SDL_Window>;

		OVGFWindow() = default;

		OVGFWindow(OVGFSettings&) noexcept;
		~OVGFWindow() noexcept;

		void get_window_extentions(uint32_t& count, vector<const char*>& exts) const noexcept;

		SDL_bool get_surface(VkInstance&,VkSurfaceKHR&) noexcept;

	private:
		ovgf_uptr_t<SDL_Window> window;
	};
}

#endif