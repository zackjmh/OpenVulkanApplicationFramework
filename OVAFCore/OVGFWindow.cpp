#include "OVGFWindow.h"
#include <cstdio>
#include <cassert>
#include "OVGF_Logger.h"

namespace OVGF {
	using IH_Utils::LogLevel;

	string OVGF_SDL_GetError(const string& pre) noexcept {
		return pre + (string)SDL_GetError();
	}


	OVGFWindow::OVGFWindow(OVGFSettings& set) noexcept {
		if (SDL_Init(SDL_INIT_VIDEO)) {
			OVGF_Logger(OVGF_SDL_GetError("SDL_Init Error: "), LogLevel::FATAL);
			abort();
		}


		window = ovgf_make_unique(SDL_CreateWindow(set.app_name.c_str(), 
			                                           set.window_x, 
			                                           set.window_y, 
			                                           set.window_w, 
			                                           set.window_h, 
                                                       set.window_flags | SDL_WINDOW_VULKAN));

		if (!window) {
			OVGF_Logger(OVGF_SDL_GetError("Window was nullptr: "), LogLevel::CRITICAL);
			abort();
		}

	}

	OVGFWindow::~OVGFWindow() noexcept {
		SDL_Quit();
	}

	void OVGFWindow::get_window_extentions(uint32_t& count, vector<const char*>& exts) const noexcept {
		SDL_bool res = SDL_Vulkan_GetInstanceExtensions(window.get(), &count, nullptr);

#ifndef NDEBUG
		if (!res) {
			OVGF_Logger(OVGF_SDL_GetError("Failed to get required extention count "), LogLevel::CRITICAL);
		}

#endif // !NDEBUG


		exts.resize(count);

		res = SDL_Vulkan_GetInstanceExtensions(window.get(), &count, exts.data());

#ifndef NDEBUG
		if (!res) {
			OVGF_Logger(OVGF_SDL_GetError("Failed to get required extentions "), LogLevel::CRITICAL);
		}
#endif // !NDEBUG
	}

	SDL_bool OVGFWindow::get_surface(VkInstance& inst, VkSurfaceKHR& out) noexcept {
		return SDL_Vulkan_CreateSurface(window.get(), inst, &out);
	}
}