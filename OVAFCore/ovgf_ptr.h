#pragma once
#ifndef OVGF_PTR_H
#define OVGF_PTR_H
#include <SDL.h>
#include <memory>

namespace OVGF {
	struct ovgf_deleter {

		template<typename T>
		void operator()(T* t) {
			ovgf_delete(std::forward<T*>(t));
		}

		
		void ovgf_delete(SDL_Window* w) {
			SDL_DestroyWindow(w);
		}
	};


	template<typename T>
	using ovgf_uptr_t = std::unique_ptr<T, ovgf_deleter>;


	template<typename T, typename... Args>
	ovgf_uptr_t<T> ovgf_make_unique(Args&&... args) {
		return ovgf_uptr_t<T>(new T(std::forward<Args>(args)...));
	}

	template<typename T>
	ovgf_uptr_t<T> ovgf_make_unique(T* args) {
		return ovgf_uptr_t<T>(std::forward<T*>(args));
	}

	typedef nullptr_t OVGF_ALLOCATOR_t;
	static OVGF_ALLOCATOR_t OVGF_ALLOCATOR = nullptr;
}
#endif