#ifndef OVGF_CONFIG_H
#define OVGF_CONFIG_H

#include <unordered_map>
#include <string>
#include <vector>
#include "OVGF_Logger.h"


namespace OVGF {

	using IH_Utils::LogLevel;
	using std::string;
	using std::unordered_map;
	using std::vector;
	using std::pair;
	using std::forward;

	typedef unordered_map<string, string> SettingsMap;

	enum class ESettings {
		APP_NAME,
		WINDOW_X,
		WINDOW_Y,
		WINDOW_H,
		WINDOW_W,
		VERSION,
		WINDOW_FLAGS,
		UNKNOWN
	};

	struct SettingsList {
		string name;
		ESettings value;

	};

	//TODO: Figure out the compiletime expressions required to actually parse the config file via MP

	inline vector<SettingsList> req_s_list{
	{ "app_name",     ESettings::APP_NAME },
	{ "window_x",     ESettings::WINDOW_X },
	{ "window_y",     ESettings::WINDOW_Y },
	{ "window_h",     ESettings::WINDOW_H },
	{ "window_w",     ESettings::WINDOW_W },
	{ "version",      ESettings::VERSION },
	{ "window_flags", ESettings::WINDOW_FLAGS }};

	inline vector<SettingsList> s_list{
	{ "UNKOWN",       ESettings::UNKNOWN } };

	inline vector<SettingsList> get_settings() {
		static vector<SettingsList> out{ req_s_list.begin(), req_s_list.end() };
		if (out.back().value != s_list.back().value)
			out.insert(out.end(), s_list.begin(), s_list.end());

		return out;
	}

	inline ESettings value(const string& name, vector<SettingsList> entries) {
		for (const auto& setting : entries) {
			if (setting.name == name)
				return setting.value;
		}
		return ESettings::UNKNOWN;
	}

	struct OVGFSettings {
		typedef int32_t wdata_t;

		OVGFSettings(SettingsMap&& settings_) {
			if (is_complete(settings_)) {
				for (const auto& p : settings_) {
					set_value(p.first, p.second);
				}
			}
		};

		struct Version {
			typedef uint32_t version_t;

			version_t major;
			version_t minor;
			version_t rev;
		};

		struct DeviceSettings {

		};

		string   app_name;
		wdata_t  window_x;
		wdata_t  window_y;
		wdata_t  window_w;
		wdata_t  window_h;
		uint32_t window_flags;
		Version  version;
		DeviceSettings device_reqs;

	private:
		bool set_value(const string& setting_, const string& val) {

			switch (value(setting_, get_settings())) {
			case ESettings::APP_NAME:
				app_name = val;
				break;
			case ESettings::VERSION:
				version = make_version(val);
				break;
			case ESettings::WINDOW_FLAGS:
				window_flags = parse_int(val, setting_);
				break;
			case ESettings::WINDOW_H:
				window_h = parse_int(val, setting_);
				break;
			case ESettings::WINDOW_X:
				window_x = parse_int(val, setting_);
				break;
			case ESettings::WINDOW_Y:
				window_y = parse_int(val, setting_);
				break;
			case ESettings::WINDOW_W:
				window_w = parse_int(val, setting_);
				break;
			default:
				OVGF_Logger("Unparseable config:" + setting_ + val, LogLevel::STERN_WARNING);
				return false;
			}

			return true;
		};

		int parse_int(string to_p, string setting_) {
			if (to_p == "") {
				OVGF_Logger("Setting " + setting_ + " was null.", LogLevel::CRITICAL);
				return 0;
			} else {
				return stoi(to_p);
			}
		};

		Version make_version(string ver_string) {
			auto ConsumeVerstring = [&]() {

				Version::version_t out = stoi(ver_string.substr(0, ver_string.find('.')));
				ver_string = ver_string.substr(0, ver_string.find('.') + 1);

				return out;
			};

			return Version{ ConsumeVerstring(), ConsumeVerstring(), ConsumeVerstring() };
		}

		bool is_complete(SettingsMap& settings_) {
			for (const auto& s : req_s_list) {
				if (!settings_.count(s.name)) {
					return false;
				}
			}

			return true;
		}
	};



}
#endif // !OVGF_CONFIG_H
