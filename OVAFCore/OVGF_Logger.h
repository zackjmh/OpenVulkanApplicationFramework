#pragma once
#ifndef OVGF_LOGGER_H
#define OVGF_LOGGER_H
#include <Logger.h>
#include <string>

namespace OVGF {
	using std::string;
	using IH_Utils::LogLevel;
	using IH_Utils::Logger;

	inline void OVGF_Logger(string message, 
		LogLevel level = LogLevel::DETAIL) {
#ifndef NLOG
		(Logger::get_logger("ErrorLog.txt"))(move(message), level);
#endif
	}
}
#endif // !OVGF_LOGGER
