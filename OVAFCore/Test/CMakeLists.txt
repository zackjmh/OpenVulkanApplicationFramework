 ADD_EXECUTABLE(OVGFTest Test_Driver.cpp)
 
 CONFIGURE_FILE($ENV{SDL2DIR}\\lib64\\SDL2.dll 
                ${CMAKE_CURRENT_BINARY_DIR} COPYONLY)

add_custom_command(TARGET OVGFTest POST_BUILD
                   COMMAND ${CMAKE_COMMAND} -E copy_directory
                       ${CMAKE_CURRENT_SOURCE_DIR}\\..\\config\\
					   ${CMAKE_CURRENT_BINARY_DIR}\\config\\)

 TARGET_INCLUDE_DIRECTORIES(OVGFTest PUBLIC 
								OpenVulkanGameFramework 
								${SDL2_INCLUDE_DIR})

 TARGET_LINK_LIBRARIES(OVGFTest PUBLIC
							OpenVulkanGameFramework 
							${SDL2_LIBRARY}
							${Vulkan_LIBRARY})