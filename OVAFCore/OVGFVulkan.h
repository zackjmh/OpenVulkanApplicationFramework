#pragma once
#ifndef OVGF_VULKAN_H
#define OVGF_VULKAN_H
#include <vector>

#include <vulkan/vulkan.hpp>
#include "OVGFWindow.h"
#include "OVGFDevice.h"
#include "OVGFConfig.h"

namespace OVGF {

	using std::string;
	using std::vector;
	typedef uint32_t count_t;


	class OVGFVulkan {
	public:
		OVGFVulkan() = default;
		OVGFVulkan(OVGFWindow&, OVGFSettings&) noexcept;
		~OVGFVulkan() noexcept;


	private:
		VkInstance               instance;
		VkSurfaceKHR             surface;
		count_t                  device_count = 0;
		vector<OVGFDevice>       device;

#ifndef NDEBUG
		VkDebugUtilsMessengerEXT callback;

		void setup_OVGF_debug_callback() noexcept;
#endif

		VkResult make_instance(OVGFWindow&,string&,OVGFSettings::Version&) noexcept;
		vector<OVGFDevice> get_devices(OVGFSettings&) noexcept;

	};
}
#endif
