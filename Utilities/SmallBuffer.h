#ifndef SMALL_BUFFER_H
#define SMALL_BUFFER_H
#include <memory>
#include <array>
#include <typeindex>

namespace IH_Utils {
	
	using std::aligned_storage_t;
	using std::type_index;

	using std::enable_if_t;
	using std::forward;
	using std::decay_t;

	class SmallBuffer {
	private:

		const static size_t BIG_VAL = 64; //Only allow types that fit on a cache line
		template<typename T>
		using EnableIfSmall = enable_if_t<(sizeof(T) < BIG_VAL)>;

	public:
		inline SmallBuffer() : buffer(), type(typeid(nullptr)) {
		};

		template <typename T, typename = EnableIfSmall<T>, typename dtype = decay_t<T>>
		inline SmallBuffer(T&& t) : type{ typeid(dtype) } {
			new (&buffer) dtype{ forward<T>(t) };
		};

		virtual ~SmallBuffer() noexcept {
		};

		inline const type_index get_type() const {
			return type;
		}

	protected:
		typedef SmallBuffer super;
		aligned_storage_t<BIG_VAL> buffer;
		const type_index type;
	};
}
#endif