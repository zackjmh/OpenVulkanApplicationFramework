#pragma once
#ifndef IH_UTILS_CALC_H
#define IH_UTILS_CALC_H

#include <string>
#include <chrono>
#include <ratio>

namespace IH_Utils {
	namespace Calc {
		
		using std::string;
		using std::to_string;

		using std::chrono::nanoseconds;
		using std::chrono::microseconds;
		using std::chrono::milliseconds;
		using std::chrono::seconds;
		using std::chrono::minutes;
		using std::chrono::duration_cast;

		using std::giga;
		using std::mega;
		using std::kilo;

		typedef size_t counter_t;
		typedef size_t jobs_t;

		string get_time(nanoseconds dur) {
			counter_t out_c = 0;
			string out = "";

			if (dur >= microseconds(1)) {
				if (duration_cast<microseconds>(dur) >= milliseconds(1)) {
					if (duration_cast<milliseconds>(dur) >= seconds(1)) {
						if (duration_cast<seconds>(dur) >= minutes(1)) {
							if (out_c < 2) {
								out += to_string(duration_cast<minutes>(dur).count()) + "min ";
								++out_c;
							}
						}
						if (out_c < 2) {
							out += to_string((duration_cast<seconds>(dur) - duration_cast<minutes>(dur)).count()) + "sec ";
							++out_c;
						}
					}
					if (out_c < 2) {
						out += to_string((duration_cast<milliseconds>(dur) - duration_cast<seconds>(dur)).count()) + "ms ";
						++out_c;
					}
				}
				if (out_c < 2) {
					out += to_string((duration_cast<microseconds>(dur) - duration_cast<milliseconds>(dur)).count()) + "microseconds ";
					++out_c;
				}
			}
			if (out_c < 2)
				out += to_string((dur - duration_cast<microseconds>(dur)).count()) + "ns ";

			return out;
		}

		string get_ops(nanoseconds dur, jobs_t jobs) {
			double_t ops = jobs / (double_t)duration_cast<seconds>(dur).count();

			if (ops >= giga::num) {
				return to_string(ops / giga::num) + " GOPS.";
			} else if (ops >= mega::num) {
				return to_string(ops / mega::num) + " MOPS.";
			} else {
				return to_string(ops / kilo::num) + " KOPS.";
			}
		}

	}
}
#endif